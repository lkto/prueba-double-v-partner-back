<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Repository\TicketRepository;
use App\Service\TicketDeleteService;
use App\Service\TicketSaveService;
use App\Service\TicketService;
use App\Service\TicketUpdateService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class TicketController extends AbstractController
{
    /**
     * @Route("/ticket", name="ticket-index",  methods={"GET"})
     * @return Response
     */
    public function index(TicketRepository $ticketRepository, PaginatorInterface $paginator, Request $request, TicketService $ticketService): Response
    {
        $query = $ticketRepository->findAllDSC();
        $page = $request->query->get("page");
        if(!$page){
            $page = 1;
        }

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', $page), /*page number*/
            10 /*limit per page*/
        );

        $ticket = $ticketService->convertEntitiesToArray($pagination->getItems());

        return new JsonResponse($ticket, 200);
    }

    /**
     * @Route("/ticket/{ticket}", name="ticket-get-index",  methods={"GET"})
     * @return Response
     */
    public function getTicket($ticket ,TicketRepository $ticketRepository): Response
    {
        $data = $ticketRepository->findOneById($ticket);
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/ticket/create", name="ticket-create",  methods={"POST"})
     * @param Request $request
     * @param TicketSaveService $saveService
     * @return Response
     */
    public function create(Request $request, TicketSaveService $saveService): Response
    {
        $ticket = json_decode($request->getContent(), true);
        $data = $saveService->save($ticket);
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/ticket/update", name="ticket-update",  methods={"POST"})
     * @param Request $request
     * @param TicketSaveService $saveService
     * @return Response
     */
    public function update(Request $request, TicketUpdateService $updateService): Response
    {
        $ticket = json_decode($request->getContent(), true);
        $data = $updateService->update($ticket);
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/ticket/delete/{ticket}", name="ticket-delete",  methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function delete($ticket, TicketDeleteService $deleteService): Response
    {
        $data = $deleteService->delete($ticket);
        return new JsonResponse($data, 200);
    }
}
