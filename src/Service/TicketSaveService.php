<?php

namespace App\Service;

use App\Repository\TicketRepository;

class TicketSaveService
{
    private TicketService $ticketService;
    private TicketRepository $ticketRepository;

    /**
     * @param TicketService $ticketService
     * @param TicketRepository $ticketRepository
     */
    public function __construct(TicketService $ticketService, TicketRepository $ticketRepository)
    {
        $this->ticketService = $ticketService;
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * @param $ticket
     * @return array
     */
    public function save($ticket)
    {
        $error = false;
        $message = '';

        try {
            $data = $this->ticketService->convertArrayToEntity($ticket);
            if(!$data['error']){
                $this->ticketRepository->save($data['data'], true);
                $message = "Registro creado satisfactoriamente";
            }else{
                $error = true;
                $message = $data['message'];
            }
        }catch (\Exception $exception){
            $error = true;
            $message = $exception->getMessage();
        }

        return [
            'message' => $message,
            'error' => $error,
        ];

    }
}