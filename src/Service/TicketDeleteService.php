<?php

namespace App\Service;

use App\Entity\Ticket;
use App\Repository\TicketRepository;

class TicketDeleteService
{
    private TicketRepository $ticketRepository;

    /**
     * @param TicketRepository $ticketRepository
     */
    public function __construct( TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function delete($ticket)
    {
        $error = false;
        $message = '';

        try {
            $ticketEntity = $this->ticketRepository->findOneBy(['id' => $ticket]);
            if($ticketEntity){
                $this->ticketRepository->remove($ticketEntity, true);
                $message = "Registro eliminado satisfactoriamente";
            }else{
                $error = true;
                $message = "El ticket no se encuentra registrado, verifique la informacion";
            }

        }catch (\Exception $exception){
            $error = true;
            $message = $exception->getMessage();
        }


        return [
            'message' => $message,
            'error' => $error,
        ];
    }
}