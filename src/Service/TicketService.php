<?php

namespace App\Service;

use App\Entity\Ticket;

class TicketService
{
    /**
     * @param $data
     * @return array
     */
    public function  convertArrayToEntity($data): array
    {
        $error = false;
        $message = '';
        $ticket = new Ticket();
        try {
            $date = new \DateTime($data['createdAt']);
            $ticket->setUser($data['user']);
            $ticket->setMessage($data['message']);
            $ticket->setCreatedAt($date);
            $ticket->setState($data['state']);

        }catch (\Exception $exception){
            $error = true;
            $message = $exception->getMessage();
        }

        return [
            'message' => $message,
            'error' => $error,
            'data' => $ticket
        ];
    }

    public function convertEntitiesToArray(array $data): array
    {
        $ticket = [];

        /** @var $ticketEntity Ticket  */
        foreach ($data as $ticketEntity)
        {
            $ticketTemp = [
                "id" => $ticketEntity->getId(),
                "user" => $ticketEntity->getUser(),
                "message" => $ticketEntity->getMessage(),
                "createdAt" => $ticketEntity->getCreatedAt()->format('Y-m-d H:i:s'),
                "updatedAt" => $ticketEntity->getUpdatedAt()?->format('Y-m-d H:i:s'),
                "state" => $ticketEntity->getState()
            ];

            $ticket[] = $ticketTemp;
        }

        return $ticket;
    }
}