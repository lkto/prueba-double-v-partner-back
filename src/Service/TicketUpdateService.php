<?php

namespace App\Service;

use App\Repository\TicketRepository;

class TicketUpdateService
{
    private TicketService $ticketService;
    private TicketRepository $ticketRepository;

    /**
     * @param TicketService $ticketService
     * @param TicketRepository $ticketRepository
     */
    public function __construct(TicketService $ticketService, TicketRepository $ticketRepository)
    {
        $this->ticketService = $ticketService;
        $this->ticketRepository = $ticketRepository;
    }

    public function update($ticket)
    {
        $error = false;
        $message = '';

        try {
            $ticketEntity = $this->ticketRepository->findOneBy(['id' => $ticket['id']]);
            if($ticketEntity){
                $ticketEntity->setMessage($ticket['message']);
                $ticketEntity->setUser($ticket['user']);
                $ticketEntity->setState($ticket['state']);

                $this->ticketRepository->save($ticketEntity, true);

                $message = "Registro modificado satisfactoriamente";

            }else{
                $error = true;
                $message = "El ticket no se encuentra registrado, verifique la informacion";
            }
        }catch (\Exception $exception){
            $error = true;
            $message = $exception->getMessage();
        }


        return [
            'message' => $message,
            'error' => $error,
        ];
    }

}