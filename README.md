# Prueba de php

## Requerimientos
 - PHP 8.1.0
 - Composer 2.3.7
 - Symfony CLI version v4.28.1

## Instalacion

Una vez descargado el proyecto es necesario correr en la consola

- composer install (Para instalar los Vendors)
- php bin/console doctrine:database:create (para crear la bd)
- php bin/console doctrine:schema:update --force (para crear tablas y  columnas)

- para correr el proyecto en la consola correr: symfony server:start


## Informacion

El proyecto cuenta con 5 endpoints.

- Crear tickets:
    - Api : http://127.0.0.1:8000/api/ticket/create
    - Method: POST
    - Json example :     {
      "user": "Emilson",
      "message": "Prueba 4",
      "createdAt": "2023-02-15 08:22:20",
      "state": true
    }


- Modificar tickets:
  - Api : http://127.0.0.1:8000/api/ticket/update
  - Method: POST
  - Json example : {
    "id": 4,
    "user": "Emilson Bejarano",
    "message": "Prueba 4",
    "createdAt": "2023-02-15 08:22:20",
    "state": true
  }


- Eliminar tickets:
  - Api : http://127.0.0.1:8000/api/ticket/delete/{id}
  - Method: GET


- Listar tickets:
    - Api : http://127.0.0.1:8000/api/ticket
    - Api con Paginacion : http://127.0.0.1:8000/api/ticket?page=1
    - Method : GET


- Buscar ticket:
  - Api : http://127.0.0.1:8000/api/ticket/{id}
  - Method : GET


## NOTA 
Si no puede ejecutar los comandos anteriores el proyecto no funcionara pongase en contacto al email: ingeniero.ember.bejarano@gmail.com o al wpp: 3117857328 para dar ejecucion al proyecto


